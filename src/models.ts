export interface IPlayer {
    name: string;
}

export interface IResponse {
    positionX: number;
    positionY: number;
    message: string;
    options: any;
}

export interface IAction {
    path: string;
    method: string;
    body?: any;
}
