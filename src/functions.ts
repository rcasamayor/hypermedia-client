import { IResponse } from "./models";

export function showTitle(title: string) {
    for (let i = 0; i < title.length + 4; i++) process.stdout.write("*");
    console.log("\n* %s *", title);
    for (let i = 0; i < title.length + 4; i++) process.stdout.write("*");
    console.log("\n\n");
}

export function showResponse(json: IResponse) {
    console.log("Coordinates: { x: %d, y: %d }", json["positionX"], json["positionY"]);
    console.log("Hint: %s\n", json["message"]);

    console.log("Available actions:");
    for (let i = 0; i < json["options"].length; ++i) {
        console.log("  [%d] - %s", i, JSON.stringify(json["options"][i]));
    }
}
