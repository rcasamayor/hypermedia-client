const reader = require("readline-sync");
const request = require("node-fetch");

import { showResponse, showTitle } from "./functions";
import { IAction, IPlayer } from "./models";

const url: string = "https://webhook.site/ca11411d-5833-49a3-a984-bc4a36794e2d";

async function main() {
    // Show the game title.
    showTitle("Hypermedia's Treasure Quest");

    // Ask the player for his/her name.
    const player: IPlayer = {
        name: reader.question("What is your name? "),
    };
    console.log(`└ Welcome to the game, ${player.name}.\n`);

    // TODO: Check if the player already exists. If so, retrieve the last status of the player available on the game server.
    // Register the player on the game server.
    try {
        const response = await request(url, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(player),
        });
        const json = await response.text();
        console.log(json);
    } catch (error) {
        console.log(error);
    }

    /**
     * MOCK
     *
     * After a request, the game server responds with the next JSON.
     * The information is shown to the player on the screen.
     */
    const json = {
        positionX: 0,
        positionY: 2,
        message: "You found a shovel. Do you want to pick it up?",
        options: [
            {
                info: "Pick up shovel",
                path: "/player/:name/shovel",
                method: "GET",
            },
            {
                info: "Move east",
                path: "/player/:name",
                method: "PUT",
                body: {
                    direction: "east",
                },
            },
            {
                info: "Move north",
                path: "/player/:name",
                method: "PUT",
                body: {
                    direction: "north",
                },
            },
            {
                info: "Move south",
                path: "/player/:name",
                method: "PUT",
                body: {
                    direction: "south",
                },
            },
        ],
    };

    // TODO: Game loop.

    // Show game server response to the player.
    showResponse(json);

    // Let the player select the following action.
    let selected: number = reader.question(`Choose an action (0-${json["options"].length - 1}) `, {
        limit: new RegExp(`[0-${json["options"].length - 1}]`),
        limitMessage: "Input valid number, please.",
    });
    selected = selected < 0 ? -selected : selected;
    console.log("\nYou have selected: %s", json["options"][selected].info);

    // Perform action selected by the player.
    const action: IAction = {
        path: json["options"][selected].path.replace(":name", player.name),
        method: json["options"][selected].method,
        body: json["options"][selected].body,
    };

    try {
        const response = await request(url, {
            // TODO: Join request 'url' with action.path.
            method: action.method,
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(action.body),
        });
        const json = await response.text();
        console.log(json);
    } catch (error) {
        console.log(error);
    }
}

main();

//// POST HTTP request
// try {
//     const response = await request(url, {
//         method: "POST",
//         headers: { "Content-Type": "application/json" },
//         body: JSON.stringify(player),
//     });
//     const json = await response.text();
//     console.log(json);
// } catch (error) {
//     console.log(error);
// }

//// GET HTTP request
// try {
//     const response = await request(url);
//     const json = await response.json();
//     console.log(json);
// } catch (error) {
//     console.log(error);
// }

//// PUT HTTP request
// try {
//     const response = await request(url, {
//         method: "PUT",
//         headers: { "Content-Type": "application/json" },
//         body: JSON.stringify({ direction: "north" }),
//     });
//     const json = await response.text();
//     console.log(json);
// } catch (error) {
//     console.log(error);
// }
